<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Log;
use Intervention\Image\Facades\Image;

class Employee extends Model
{
    public $img;
    public $ext;
    protected $table = 'employees';

    public static function boot() {
        Employee::observe(EmployeeObserver::class);
    }

    public function department() {
        return $this->belongsTo(Department::class, "departmentId");
    }
}

class EmployeeObserver {
    private function cleanBase64($str) {
        $cleaned = str_replace('data:image/png;base64,', '', $str);
        $cleaned = str_replace('data:image/jpeg;base64,', '', $cleaned);
        return str_replace(' ', '+', $cleaned);
    }

    public function retrieved(Employee $employee) {
        if ($employee->photo) {
            $path = "public" . DIRECTORY_SEPARATOR . $employee->photo;
            $type = pathinfo($path, PATHINFO_EXTENSION);
            $base64 = 'data:image/' . $type . ';base64,' . base64_encode(Storage::get($path));
            $employee->photo = $base64;
        }
        else {
            $employee->photo = "";
        }
    }

    public function saving(Employee $employee) {
        if ($employee->photo) {
            Storage::delete('public' . DIRECTORY_SEPARATOR . $employee->photo);
        }
        if (isset($employee->img['data'])){
            $img = $this->cleanBase64($employee->img['data']);
            // Yes, there can be security issues =)
            $imgFilename = $employee->id . "-" . $employee->img['name'];
            $path = storage_path() . DIRECTORY_SEPARATOR . "app" . DIRECTORY_SEPARATOR . "public" . DIRECTORY_SEPARATOR . $imgFilename;
            Image::make(base64_decode($img))->save($path);
            $employee->photo = $imgFilename;
        }
    }
}
