<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 24.06.2018
 * Time: 14:36
 */

namespace App\Http\Controllers;


use App\Employee;
use Illuminate\Http\Response;
use App\Department;

class DepartmentsController extends Controller {

    public function getAllDepartments() {
        $allDepartments = Department::with('employees')->get();
        return response()->json($allDepartments);
    }

    public function getEmployees($id) {
        $employees = Employee::where('departmentId', $id)->get();
        return response()->json($employees);
    }

}