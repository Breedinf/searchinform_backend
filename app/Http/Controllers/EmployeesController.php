<?php
/**
 * Created by PhpStorm.
 * User: Антон Матюшин
 * Date: 24.06.2018
 * Time: 14:51
 */

namespace App\Http\Controllers;
use App\Employee;
use Illuminate\Http\Request;


class EmployeesController extends Controller {

    public function updateEmployee(Request $request, $id) {
        $employee = Employee::where('id', $id)->firstOrFail();
        $employeeData = json_decode($request->getContent(), true);
        $employee->img = $employeeData['img'];
        $employee->save();
        return $this->getEmployee($id);
    }

    public function getEmployee($id) {
        $employees = Employee::where('id', $id)->firstOrFail();
        return response()->json($employees);
    }

}