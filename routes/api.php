<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('api')->group(function () {
    Route::get('/departments', 'DepartmentsController@getAllDepartments');
    Route::get('/departments/{id}/employees', 'DepartmentsController@getEmployees');
    Route::get('/employees/{id}', 'EmployeesController@getEmployee');
    Route::post('/employees/{id}', 'EmployeesController@updateEmployee');
    Route::get('/ping', function () {
        return response()->json(["result" => "ok"]);
    });
});
