<?php

use Illuminate\Database\Seeder;
use App\Department;
use App\Employee;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    private $names = ['John', 'Max', 'Ivan', 'Vasiliy', 'Alex', 'Ignat', 'Aristarkh', 'Potap'];
    private $surnames = ['Watson', 'Rabinovich', 'Kuznetsov', 'Poddubniy', 'Ivanov', 'Kacman', 'Ignatov'];
    private $departmentCount = 5;
    private $employeesCount = 125;

    public function run()
    {
        $departments = [];
        for ($depId = 0; $depId < $this->departmentCount; $depId++) {
            $dep = new Department();
            $dep->name = 'dep' . $depId;
            $dep->save();
            $departments[] = $dep;
        }

        for ($empId = 0; $empId < rand(10, $this->employeesCount); $empId++) {
            $emp = new Employee();
            $emp->name = $this->names[rand(0, count($this->names) - 1)] . ' ' . $this->surnames[rand(0, count($this->surnames) - 1)];
            $str = "";
            foreach (range(0, 10) as $number) {
                $str .= rand(0, 9);
            }
            $emp->phone = '+' . $str;
            $emp->departmentId = $departments[rand(0, $this->departmentCount - 1)]->id;
            $emp->save();
        }
    }
}
